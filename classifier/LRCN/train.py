from keras.callbacks import  ModelCheckpoint, EarlyStopping, History


def calculate_class_weights(train_label):
    list = train_label.tolist()
    num_neg = list.count(0)
    num_pos = list.count(1)

    class_weights={0 : (num_pos/num_neg) , 1: (num_pos/num_pos)}
    return class_weights

def fit(model, train_data, train_label, num_epoch, patience, batch_size, val_split, class_weight_enabled):
    print "Training"
    model.load_weights("./models/clear_model.h5")
    class_weights = calculate_class_weights(train_label)
    history = History()
    best_model = ModelCheckpoint("./models/best_model.h5", monitor="val_loss", save_best_only=True)
    early_stopper = EarlyStopping(monitor="val_loss", patience=patience)
    model.fit(train_data, train_label,
              batch_size=batch_size,
              nb_epoch=num_epoch,
              verbose=0,
              shuffle=True,
              validation_split=val_split,
              class_weight=class_weights,
              callbacks=[early_stopper, best_model, history])
    model.load_weights("./models/best_model.h5")
    min_loss = min(history.history["val_loss"])
    min_loss_epoch = history.history["val_loss"].index(min_loss)
    print "Minimum Validation Loss", min_loss, "on epoch", min_loss_epoch 
    return model

