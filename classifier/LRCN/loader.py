import cv2, numpy as np, os, csv


def load_labels_and_file_name(tsv_file, directory):
    tsv_list = []
    with open(tsv_file, "rb") as tsv:
        reader = csv.reader(tsv, delimiter="\t", lineterminator="\n")
        for files in reader:
            if os.path.isfile(directory+"/"+files[0]) == True:
                tsv_list.append(files)
            else:
                pass
        return tsv_list

def load_video_vector(directory, tsv_file, height, width, number_of_timesteps):
    labels_file_name = load_labels_and_file_name(tsv_file, directory)
    number_of_videos = len(labels_file_name)
    n_channels = 1 #grayscale
    data = np.zeros((number_of_videos, number_of_timesteps, n_channels, height, width), dtype="float32")
    data.flatten()
    labels = np.empty((number_of_videos, ), dtype=np.dtype("int32"))
    for i, details in enumerate(labels_file_name):
        video = cv2.VideoCapture(directory+details[0])
        for x in range(number_of_timesteps):
            return_value, frame = video.read()
            if return_value == False:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # Grayscale
            frame = cv2.GaussianBlur(frame, (5,5) , 0)
            frame = frame[90:420, 10:600]
            frame = cv2.resize(frame, (width, height), interpolation = cv2.INTER_CUBIC)
            frame = cv2.equalizeHist(frame)
            data[i, x, :, :] = frame
        labels[i] = details[1]
    return data, labels
