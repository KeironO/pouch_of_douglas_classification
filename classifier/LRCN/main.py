import os, loader, lrcn, train as t_model, report, visualise as vis, datetime, numpy as np, warnings
from sklearn.cross_validation import LeaveOneOut

warnings.filterwarnings("ignore")

os.environ["THEANO_FLAGS"] = "mode=FAST_RUN, device=gpu, floatX=float32"

def load_data(data_dir, tsv_file, num_timesteps, height, width):
    print "Loading TVS video data..."
    data, labels = loader.load_video_vector(data_dir, tsv_file, height, width, num_timesteps)
    print "\t TVS video data successfully loaded in shape of: ", data.shape
    return data, labels

def initalise_base_model(num_timesteps, height, width, num_channels, num_filters, num_conv, pool_size, optimiser, strides):
    model, convout = lrcn.create_lcrn(num_timesteps, height, width, num_channels, num_filters, num_conv, pool_size, optimiser, strides)
    return model, convout

def evaluate_model(test_data):
    eval_prob = model.predict_proba(test_data, verbose=0)
    eval_pred = model.predict_classes(test_data, verbose=0).tolist()

    return eval_prob, eval_pred

def train_and_val(model, num_folds, data, labels, num_epoch, patience, batch_size, val_split, visualise, convout, class_weight_enabled):
    loo = LeaveOneOut(len(labels))

    test_labels = []
    predictions = []
    probabilities = []

    folder_name = "./results/"+str(datetime.datetime.now().date()) + str(datetime.datetime.now().time())
    if not os.path.exists(str(folder_name)):
            os.makedirs(str(folder_name))
    open(folder_name+"/model.json", "w").write(model.to_json())
    for i, (train, test) in enumerate(loo):
        model = t_model.fit(model, data[train], labels[train], num_epoch, patience, batch_size, val_split, class_weight_enabled)
        eval_prob, eval_pred = evaluate_model(data[test])
        test_labels.extend(labels[test])
        predictions.extend(eval_pred)
        probabilities.extend(eval_prob)
        vis.main(model, convout, data[test], labels[test], folder_name)
        if visualise == True:
            vis.visualise_comparison(model, convout, data[test], labels[test])
        else:
            pass
    return model, test_labels, predictions, probabilities, folder_name

if __name__ == "__main__":
    # Data-related Parameters

    data_dir = "/home/keo7/.data/pouch_of_douglas/lim/"
    tsv_file = "/home/keo7/.data/pouch_of_douglas/labels.tsv"

    num_timesteps, height, width = 53, 30, 40
    num_channels = 1

    # Network-related Parameters

    num_filters = 3
    num_conv = 8
    pool_size = 6
    pool_strides = 1
    np.random.seed(1337)
    optimiser = "Adam"

    # Training-related Parameters

    num_epoch = 1000
    num_folds = 10 
    patience = 150
    batch_size = 8
    val_split = ((0.1 / num_folds) * 10)
    class_weight_enabled = False

    # Evaluation-related Parameters
    visualise = True

    # Report-related Parameters
    roc = True

    data, labels = load_data(data_dir, tsv_file, num_timesteps, height, width)
    model, convout = initalise_base_model(num_timesteps, height, width, num_channels, num_filters, num_conv, pool_size, optimiser, pool_strides)
    model, test_labels, predictions, probabilities, folder_name = train_and_val(model, num_folds, data, labels, num_epoch, patience, batch_size, val_split, visualise, convout, class_weight_enabled)
    report.return_report(roc, folder_name, test_labels, predictions, probabilities)
