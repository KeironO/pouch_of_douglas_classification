from keras.models import Sequential
from keras.layers.extra import TimeDistributedFlatten, TimeDistributedConvolution2D, TimeDistributedMaxPooling2D
from keras.layers.core import Dense, Activation, Dropout
from keras.optimizers import SGD, RMSprop, Adam, Adadelta
from keras.layers.recurrent import  GRU, LSTM
#from keras.utils.layer_utils import model_summary

def create_lcrn(num_of_timesteps, height, width, num_channels, num_filters, num_conv, pool_size, optimiser, stride):
    model = Sequential()

    # Convolutional Layers

    model.add(TimeDistributedConvolution2D(num_filters,num_conv,num_conv, border_mode="same",
                            input_shape=(num_of_timesteps,num_channels,height,width)))
    # For visualisation purposes
    convout1 = Activation('relu')
    model.add(convout1)

    model.add(TimeDistributedMaxPooling2D(pool_size=(pool_size,pool_size), strides=(stride, stride)))
    model.add(Dropout(0.25))

    model.add(TimeDistributedFlatten())

    model.add(LSTM(64, return_sequences=True))
    model.add(LSTM(32, return_sequences=False))

    model.add(Dense(1))
    model.add(Activation("sigmoid"))
    
    if optimiser == "Adam":
        o = Adam()
    elif optimiser == "RMSprop":
        o = RMSprop()
    elif optimiser == "Adadelta":
        o = Adadelta()
    else:
        o = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

    model.compile(loss="binary_crossentropy", optimizer=o, class_mode="binary")
    print "\t Model Created!"
    model.save_weights("./models/clear_model.h5", overwrite=True)

#    print model_summary(model)
    return model, convout1
