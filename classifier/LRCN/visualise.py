import os, scipy, numpy as np, matplotlib.pyplot as plt, matplotlib.cm as cm
from keras import backend as K

def visualise_comparison(model, convout1, test_data, test_label):
    print "Visualising First Acitivation Layer"
    get_layer_output = K.function([model.layers[0].input], [convout1.get_output(train=False)])
    layer_output = get_layer_output([test_data])[0]

    f, (ax1, ax2) = plt.subplots(1,2, sharey=True)
    plt.axis("equal")
    video_to_show = np.random.randint(test_data.shape[0])
    for i in range(layer_output.shape[1]):
        ax1.imshow(layer_output[video_to_show,i,1,:])
        original = test_data[video_to_show,i,:].reshape((test_data.shape[3],test_data.shape[4]))
        ax2.imshow(original, cmap=cm.Greys_r)
        plt.pause(0.0001)

def main(model, convout1, test_data, test_label, folder_name):
    get_layer_output = K.function([model.layers[0].input], [convout1.get_output(train=False)])
    layer_output = get_layer_output([test_data])[0]

    for i in range(layer_output.shape[0]):
        save_path = str(folder_name)+"/filters/"+str(test_label[i])+"/"+str(np.random.randint(100))+str(i)+"/"
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        for j in range(layer_output.shape[1]):
            scipy.misc.imsave(save_path+str(j)+".jpg", scipy.misc.imresize(layer_output[i,j,1,:], (50, 60)))
